<?php 
	include_once('assets/nullNeunElf_helper.php');
	include_once('assets/template_functions.php');
	defined('_JEXEC') or die;
?>
<!DOCTYPE html>
<html lang="De-de">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1" >
<?php // print($isTablet) ? '<meta name="apple-mobile-web-app-capable" content="yes">' : ' '; ?>
<?php // ACHTUNG hiermit!! print($isPhone) ? '<meta name="viewport" content="minimal-ui">' : ' '; ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<?php /*Fonts: */ ?>
	<link rel="prefetch" crossorigin as="font" type="font/ttf" href="<?php print '/templates/' . $this->template . '/fonts/rubik-regular-webfont.ttf'; ?>">
	<link rel="prefetch" crossorigin as="font" type="font/ttf" href="<?php print '/templates/' . $this->template . '/fonts/Rubik-Light.ttf'; ?>">	
	<link rel="stylesheet" type="text/css" href="<?php print '/templates/' . $this->template . '/css/normalize.css'; ?>">
	<link rel="stylesheet" type="text/css" href="<?php print '/templates/' . $this->template . '/css/styles.css'; ?>">
	<link rel="stylesheet" type="text/css" href="<?php print '/templates/' . $this->template . '/css/responsive.css';?>">
	<script src="<?php print '/templates/' . $this->template . '/js/jquery.min.js';?>" type="text/javascript"></script>	
	<jdoc:include type="head" />
	<link rel="canonical" href="<?php print JURI::root(); ?>" />
<?php /* bugherd - bei Go-Live entfernen 
	<script type='text/javascript'>
	(function (d, t) {
	  var bh = d.createElement(t), s = d.getElementsByTagName(t)[0];
	  bh.type = 'text/javascript';
	  bh.src = 'https://www.bugherd.com/sidebarv2.js?apikey=chodixe5rkzrbn2cpxqf3q';
	  s.parentNode.insertBefore(bh, s);
	  })(document, 'script');
	</script>
	*/ ?>
</head>
	<body id="body" class="site no-barba <?php print $agent . (!$isDesktop ? 'mobile ' : ' ') . $browser . $pageclass; ?>">
		<div id="branding">
			<h2><a onclick="menueStartReverse()" href="/">0911-Webdesign</a></h2>			
		</div>
		<div id="menuContainer">
			<div id="kontaktWrapper">
				<div class="close-kontaktIcons">&Cross;</div>
				<a class="link-kontaktIcons close-kontaktIcons--link no-barba subPageTransLink" href="/kontakt.html"></a>
				<a class="link-telefonIcon close-kontaktIcons--link" href="tel:+4991125587832" title="Klicken um direkt anzurufen 0911-255 878 32"></a>
				<?php print file_get_contents(JPATH_ROOT . '/images/kontakt-icon.svg'); ?>
			</div>
			<div id="menuBtnWrapper">
				<button class="btn-toggle" id="menu-toggle">
					<div class="top line loadAni"></div>
					<div class="middle line loadAni"></div>
					<div class="bottom line loadAni"></div>
					<div class="text">Menue</div>
				</button>
			</div>
			<?php include('blocks/submenu_alt.php'); ?>
		</div>
			<div id="wrapper">
				<div id="preLoader" class="preLoader--container">
					<div class="preLoader">
						<span class="preLoader--char"></span>
						<span class="preLoader--char"></span>
						<span class="preLoader--char"></span>
						<span class="preLoader--char last"></span>
					</div>
				</div>
				<div id="svgLayout">
					<?php if ($isDesktop) {print file_get_contents(JPATH_ROOT . '/images/svg-layout.svg');} elseif($isTablet) {print file_get_contents(JPATH_ROOT . '/images/svg-layout-tablet.svg');}else {print file_get_contents(JPATH_ROOT . '/images/svg-layout-phone.svg');} ?>
					<main  id="contentMain" role="main" class="">
						<div id="barba-wrapper">
							<div class="barba-container" data-namespace="frontpage">
								<?php // <---- Start CONTENT ----> ?>		
									<?php if($isDesktop) {include('blocks/nav-dot-menu.php');} ?>
										<?/*component: */?>
											<jdoc:include type="component" />
								<?php // <---- END CONTENT -----> ?>
							</div>
						</div>
					</main>
				</div>
				<footer id="footer">
					<p class="copyRight">&copy;0911-Webdesign <?php print date('Y'); ?> </p>
					<div class="footer--container">
						<a class="no-barba subPageTransLink" href="/impressum.html">impressum</a> | 
						<a class="no-barba subPageTransLink" href="/datenschutz.html">datenschutz</a>
					</div>
				</footer>
			</div>
			<?php if($isDesktop) : ?>
				<svg version="1.1" class="svg-filters">
					<defs>
						<filter id="filter-ripple-1">
							<feImage xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/images/ripple.png" x="-251" y="-242" width="600" height="600" result="ripple"></feImage>
							<feDisplacementMap xChannelSelector="R" yChannelSelector="G" color-interpolation-filters="sRGB" in="SourceGraphic" in2="ripple" scale="0" result="dm"></feDisplacementMap>
							<feComposite operator="in" in2="ripple"></feComposite>
							<feComposite in2="SourceGraphic"></feComposite>
						</filter>
					</defs>
				</svg>				
				<?php /*<div id="polygonBckgr" class="">
					<?php print file_get_contents(JPATH_ROOT . "/images/0911-polygon-bckgr.svg"); ?>
				</div> */ ?>
			<?php endif; ?>
				<div id="svgGrid--container" class="">
					<?php print ($isDesktop) ? file_get_contents(JPATH_ROOT . "/images/hamMenu-bkgr-desktop.svg") : file_get_contents(JPATH_ROOT . "/images/hamMenu-bkgr.svg"); ?>
				</div>
				<div id="svgShape">
					<?php print file_get_contents(JPATH_ROOT. "/images/svg-shape-transition.svg"); ?>
				</div>
		<script src="<?php print '/templates/' . $this->template . '/js/TweenMax.min.js';?>" type="text/javascript"></script>
		<script src="<?php print '/templates/' . $this->template . '/js/CSSRulePlugin.min.js';?>" type="text/javascript"></script>
		<script src="<?php print '/templates/' . $this->template . '/js/DrawSVGPlugin.min.js';?>" type="text/javascript"></script>	
		<script src="<?php print '/templates/' . $this->template . '/js/SplitText.min.js';?>" type="text/javascript" ></script>
		<script src="<?php print '/templates/' . $this->template . '/js/MorphSVGPlugin.min.js';?>" type="text/javascript"></script>
					
		<script src="<?php print '/templates/' . $this->template . '/js/barba.min.js';?>" type="text/javascript"></script>
		<script src="<?php print ($isDesktop) ? '/templates/' . $this->template . '/js/main.js' : ((!$detect->isMobile() || $detect->isTablet()) ? '/templates/' . $this->template . '/js/main-mobile.js' : '/templates/' . $this->template . '/js/main-mobile-phone.js'); ?>" type="text/javascript"></script>

		<script type="text/javascript">
			$(document).ready(function() {

				<?php // barba: ?>
					Barba.Pjax.start();
					Barba.Prefetch.init();
			});
         <?php //cookie: ?>
!function(o){var e={};o.cookieMessage=function(n){"true"!=function(o){var e,n,t,a=document.cookie.split(";");for(e=0;e<a.length;e++)if(n=a[e].substr(0,a[e].indexOf("=")),t=a[e].substr(a[e].indexOf("=")+1),(n=n.replace(/^\s+|\s+$/g,""))==o)return unescape(t)}((e=o.extend({},{mainMessage:"",acceptButton:"Aceptar",expirationDays:20,backgroundColor:"#666",fontSize:"14px",fontColor:"#c7c7c7",btnBackgroundColor:"#f2a920",btnFontSize:"11px",btnFontColor:"white",linkFontColor:"#ffff00",cookieName:"cookieMessage"},n)).cookieName)&&o(document).ready(function(){var n;n='<div id="cookie-msg"><span class="msg"><p>'+e.mainMessage+'</p><a href="" class="btn-aceptar">'+e.acceptButton+"</a></span></div>",o("body").append(n),o("#cookie-msg").css({position:"fixed",top:"0",width:"100%","text-align":"center",padding:"20px 0px","background-color":e.backgroundColor,color:e.fontColor,"font-size":e.fontSize}),o("#cookie-msg a").css({color:e.linkFontColor,"text-decoration":"underline"}),o("#cookie-msg a.btn-aceptar").css({padding:"5px 10px","border-radius":"2px","margin-left": "10px","background-color":e.btnBackgroundColor,color:e.btnFontColor,"font-size":e.btnFontSize,"text-decoration":"none"}),o("#cookie-msg a.btn-aceptar").on("click",function(){return function(o,e,n){var t=new Date;t.setDate(t.getDate()+n);var a=escape(e)+(null==n?"":"; expires="+t.toUTCString());document.cookie=o+"="+a}(e.cookieName,!0,e.expirationDays),o("#cookie-msg").remove(),!1})})}}(jQuery);			
		<?php /*cookieMessage*/ ?>
                jQuery.cookieMessage({
                    'mainMessage': 'Diese Webseite verwendet Cookies. Wenn Sie diese Webseite nutzen, akzeptieren Sie die Verwendung von Cookies. Weitere Informationen erhalten Sie in unserer <a class="no-barba subPageTransLink" href="/datenschutz.html">Datenschutzerklärung</a>. ',
                    'acceptButton': 'Verstanden!',
                    'fontSize': '14px',
                    'backgroundColor': 'rgb(26, 43, 53, 0.7)',
                    'btnBackgroundColor': '#484848',
                    'btnFontSize': '14px',
                    'btnFontColor': '#fff',
                    'linkFontColor': '#fff',
                    'expirationDays': 30,
                });			
		</script>
	<?php if(!$isMobile) : ?>
		<div id="resizeAlarm">
			<p>Das Fenster Ihres Webbrowsers ist zu klein - bitte vergrössern Sie ihr Browser-Fenster um die Inhalte sinnvoll darstellen zu können.</p>
		</div>
	<?php endif; ?>				
	</body>
</html>