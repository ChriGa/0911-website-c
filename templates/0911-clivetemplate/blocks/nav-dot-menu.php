<?php /*
 * by 0911 - webdesign
 * warum diesen Teil als extra include? -> weil links innerhalb foreignobjects nicht "accessible" sind wenn diese als file in der index via file_get_contents geladen werden!
 */
	defined('_JEXEC') or die;
	$currentUri = $_SERVER['REQUEST_URI'];
	switch($currentUri) : // hier die finalen relativen URL's anpassen!:
		case "/" : $home = true;
			break;
		case "/homepage-design-fuer-ihre-zielgruppe.html" : $design = true;
			break;
		case "/webseite-schneller-machen.html" : $perform = true;
			break;
		case "/homepage-konzept-und-strategie.html" : $konzept = true;
			break;
		case "/webentwicklung-und-programmierung.html" : $entwicklung = true;
			break;			
	endswitch;
?>
<div class="navDotWrapper <?php print ($is_home) ? "is_home" : "not_home"; ?>">
	<svg id="navDotDots" width="100vw" height="100vh" viewBox="0 0 1920 960" zoomAndPan="disable">
		<style type="text/css">.ellipseLinkParent.active {stroke:#2962ff}</style>
		<foreignobject x="30" y="385" width="200" height="30">
			<p class="menueTitle">| menue </p>
		</foreignobject>
			<line id="home-line" class="st-line" x1="43" y1="559" x2="43" y2="440"/>
			<line id="design-line" class="st-line" x1="79.5" y1="440" x2="79.5" y2="321"/>
			<line id="performance-line" class="st-line" x1="115.5" y1="521" x2="115.5" y2="441"/>
			<line id="konzept-line" class="st-line" x1="151.5" y1="441" x2="151.5" y2="386"/>
			<line id="entwicklung-line" class="st-line" x1="187.5" y1="520" x2="187.5" y2="440"/>						
		<g id="nav-dots-inner-kreis">
			<ellipse class="st17 ellipseLinkParent ellipseLinkParent--home <?php print ($home) ? "active" : ""; ?>" cx="43.4" cy="440.1" rx="12.1" ry="12.5"/>
				<foreignobject class="navDot-foreignobject foreign--home" x="40" y="540" width="70" height="50">
					<a onclick="menueStartReverse()" class="ellipseLink ellipseLink--home" href="/">home</a>
				</foreignobject>									
			<ellipse class="st18 ellipseLinkParent ellipseLinkParent--design <?php print ($design) ? "active" : ""; ?>" cx="79.4" cy="440.1" rx="12.1" ry="12.5"/>			
				<foreignobject class="navDot-foreignobject foreign--design" x="75" y="315" width="80" height="50">
					<a onclick="designMenuStart()" class="ellipseLink ellipseLink--design" href="/homepage-design-fuer-ihre-zielgruppe.html">design</a>
				</foreignobject>					
			<ellipse class="st18 ellipseLinkParent ellipseLinkParent--performance <?php print ($perform) ? "active" : ""; ?>" cx="115.4" cy="440.1" rx="12.1" ry="12.5"/>
				<foreignobject class="navDot-foreignobject foreign--performance" x="115" y="500" width="140" height="50">
					<a onclick="performanceMenuStart()" class="ellipseLink ellipseLink--performance" href="/webseite-schneller-machen.html">performance</a>
				</foreignobject>					
			<ellipse class="st18 ellipseLinkParent ellipseLinkParent-konzeption <?php print ($konzept) ? "active" : ""; ?>" cx="151.4" cy="440.1" rx="12.1" ry="12.5"/>
				<foreignobject class="navDot-foreignobject foreign--konzeption" x="150" y="380" width="120" height="50">
					<a onclick="konzeptionMenuStart()" class="ellipseLink ellipseLink--konzeption" href="/homepage-konzept-und-strategie.html">konzeption</a>
				</foreignobject>									
			<ellipse class="st18 ellipseLinkParent ellipseLinkParent-entwicklung <?php print ($entwicklung) ? "active" : ""; ?>" cx="187.4" cy="440.1" rx="12.1" ry="12.5"/>
				<foreignobject class="navDot-foreignobject foreign--entwicklung" x="185" y="500" width="130" height="50">
					<a onclick="entwicklungMenuStart()" class="ellipseLink ellipseLink--entwicklung" href="/webentwicklung-und-programmierung.html">entwicklung</a>					
				</foreignobject>									
		</g>			
	</svg>
	<svg id="svgFilter" width="100vw" height="100vh">
		<defs>
		    <filter id="displacementFilter">
		    	<feTurbulence id="turbulence" type="turbulence" baseFrequency="0.0"
		          numOctaves="1" result="turbulence"/>
		    	<feDisplacementMap in2="turbulence" in="SourceGraphic"
		          scale="5" xChannelSelector="R" yChannelSelector="G"/>       
		    </filter>
		</defs>  
	</svg>	
</div>
<style type="text/css">p.menueTitle {transition: all 2s linear;}</style>