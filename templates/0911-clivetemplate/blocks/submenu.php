<?php /*
 * by 0911 - webdesign
 */
  defined('_JEXEC') or die;
?>
<nav id="subMenu">
  <div class="nav--InsideContainer__split">
      <div class="nav--InsideContainer__left">
        <div class="nav--InsideContainer breadcrumb">
          <p class="marker">
            <span>&rarr;</span> main nav
          </p>
            <ul id="mainSubmenu" class="subMenu-BreadCrumb">
              <li><a class="subBreadLink home " onclick="menueStartReverse()" href="/">home</a></li>
              <li><a class="subBreadLink design <?php print ($design) ? "active" : ""; ?>" onclick="designMenuStart()" href="/design.html">design</a></li>
              <li><a class="subBreadLink perform <?php print ($perform) ? "active" : ""; ?>" onclick="performanceMenuStart()" href="/performance.html">performance</a></li>
              <li><a class="subBreadLink konzept <?php print ($konzept) ? "active" : ""; ?>" onclick="konzeptionMenuStart()"  href="/konzeption.html">konzeption</a></li>
              <li><a class="subBreadLink entwicklung <?php print ($entwicklung) ? "active" : ""; ?>" onclick="entwicklungMenuStart()" href="/entwicklung.html">entwicklung</a></li>
          </ul>
        </div>
        <div class="nav--InsideContainer blog">         
          <div class="nav--InsideContainer__img"></div>
          <a class="subMenu__list--link no-barba" href="blog.html" >Blog<br /><span>Der Web-Loves-You BLOG um alles Neue und Wissenswerte</span></a>         
        </div>
      </div>
      <div class="nav--InsideContainer__left">
        <div class="nav--InsideContainer kontakt">
          <p class="marker">
            nicht so sch&uuml;chtern 
          </p>            
        <div class="vcard">
            <p class="org">0911 Webdesign</p>
            <p class="fn">Chris Gabler</p>
            <p class="tel "><a class="" href="tel:+49911PERFECT">Tel: 0911 - 555 PERFECT</a></p>
            <p class="adr"><span class="street-address">Rauh&auml;ckerstr.16</span><br> <span class="postal-code">90431 </span><span class="region">N&uuml;rnberg</span></p>
                  <p><a class="email" href="mailto:info@0911-webdesign.com">info@0911-webdesign.com</a></p>
        </div>
        </div>
        <div class="nav--InsideContainer suche">
          <p class="marker">
            <span>&uarr;</span> entdecken!
          </p>
            <form id="" action="/suche?f=1" method="get" class="form-search ">
              <div class="finder_menu">
                <button class="btn btn-primary" type="submit" title="" data-original-title="Start"><span class="icon-search icon-white"></span> </button>
                <input type="text" name="q" id="mod-finder-searchword84" class="search-query input-medium morphsearch-input" size="20" value="" placeholder="Suche ..." autocomplete="off">
                <label for="mod-finder-searchword84" class="element-invisible finder_menu"> </label>
                <input type="hidden" name="f" value="1">	</div>
            </form>    
        </div>
      </div>
    </div>
    <div class="nav--InsideContainer__split">    
      <div class="nav--InsideContainer secondMenu">
          <p class="marker">
            <span>&rarr;</span> sub nav
          </p>        
        <ul class="subMenu__list">
            <li><a class="subMenu__list--link no-barba" href="/referenzen-portfolio-0911-webdesign.html" >Projekte<br /><span>„Wer aufh&ouml;rt besser zu werden, hat aufgeh&ouml;rt gut zu sein.“</span></a></li>
            <li><a class="subMenu__list--link no-barba" href="/fuer-startups.html" >F&uuml;r Startups<br /><span>„Aus kleinem Anfang entspringen alle Dinge.“</span></a></li>
          <li><a class="subMenu__list--link no-barba" href="/ueber-0911.html">&Uuml;ber 0911</a></li>
        </ul>  
      </div>
        <p class="marker marker--bottom">
          made with <span>&hearts;</span>
        </p>            
        <p class="down-triangle">
          <a href="#mainSubmenu">&#9660<br /><span>down</span></a>
        </p>
    </div>
</nav>