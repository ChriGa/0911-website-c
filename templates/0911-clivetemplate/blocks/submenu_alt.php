<?php /*
 * by 0911 - webdesign
 */
  defined('_JEXEC') or die;
?>
<nav id="subMenu">
  <div class="nav--InsideContainer__split">
      <div class="nav--InsideContainer__left">
        <div class="nav--InsideContainer breadcrumb">
          <p class="marker">
            <span>&rarr;</span> main nav
          </p>
            <ul id="mainSubmenu" class="subMenu-BreadCrumb">
              <li><a class="subBreadLink home " onclick="menueStartReverse()" href="/">home</a></li>
              <li><a class="subBreadLink design <?php print ($design) ? "active" : ""; ?>" onclick="designMenuStart()" href="/homepage-design-fuer-ihre-zielgruppe.html">design</a></li>
              <li><a class="subBreadLink perform <?php print ($perform) ? "active" : ""; ?>" onclick="performanceMenuStart()" href="/webseite-schneller-machen.html">performance</a></li>
              <li><a class="subBreadLink konzept <?php print ($konzept) ? "active" : ""; ?>" onclick="konzeptionMenuStart()"  href="/homepage-konzept-und-strategie.html">konzeption</a></li>
              <li><a class="subBreadLink entwicklung <?php print ($entwicklung) ? "active" : ""; ?>" onclick="entwicklungMenuStart()" href="/webentwicklung-und-programmierung.html">entwicklung</a></li>
          </ul>
        </div>
        <div class="nav--InsideContainer blog">         
          <div class="nav--InsideContainer__img"></div>
          <a class="subMenu__list--link no-barba" href="/" >Blog<br /><span>Der Web-Loves-You BLOG um alles Neue und Wissenswerte</span></a>         
        </div>
        <script>
        <?php // wenn Blog mal live entfernen: ?>        
          $(function(){
              $('div.blog a.subMenu__list--link').click("on", function(e) {
                  e.preventDefault();
              }); 
          });
        </script>
      </div>
      <div class="nav--InsideContainer__left">
        <div class="nav--InsideContainer kontakt">
          <p class="marker">
            nicht so sch&uuml;chtern 
          </p>            
        <div class="vcard">
            <p class="org">0911 Webdesign</p>
            <p class="fn">Chris Gabler</p>
            <p class="tel "><a class="" href="tel:+4991125587832">Tel: 0911 - 255 878 32</a></p>
            <p class="adr"><span class="street-address">Rauh&auml;ckerstr.16</span><br> <span class="postal-code">90431 </span><span class="region">N&uuml;rnberg</span></p>
                  <p><a class="email" href="mailto:info@0911-webdesign.com">info(at)0911-webdesign.com</a></p>
        </div>
        </div>
        <div class="nav--InsideContainer suche">
            <?php // <h3 class="search-menu-header">suche</h3> ?>
            <p class="marker">
              <span>&uarr;</span> entdecken!
            </p>
            <jdoc:include type="modules" name="menu-suche" style="none" />    
        </div>
      </div>
    </div>
    <div class="nav--InsideContainer__split">    
      <div class="nav--InsideContainer secondMenu">
          <p class="marker">
            <span>&rarr;</span> sub nav
          </p>        
        <ul class="subMenu__list">
            <li><a class="subMenu__list--link no-barba" href="https://twitter.com/0911Webdesign" target="_blank"><img src="/images/twitter-logo.png" alt="0911-Webdesign auf Twitter"/><span>0911-Webdesign auf Twitter</span></a></li>
          <li><a class="subMenu__list--link no-barba" href="/kontakt.html"><img src="/images/kontakt-icon.png" alt="eMail an 0911-Webdesign senden"/><span>Nachricht an 0911-Webdesign</span></a></li>
            <li><a class="subMenu__list--link no-barba" href="https://codepen.io/SENSENEL" target="_blank" ><img src="/images/codepen-logo.png" alt="0911-Webdesign auf Codepen"/><span>0911-Webdesign auf Codepen</span></a></li>
        </ul>  
      </div>
        <p class="marker marker--bottom">
          made with <span>&hearts;</span>
        </p>            
    </div>
<?php if($isMobile) : // entfernen wenn BLOG live geht! ?>
    <script type="text/javascript">
        $(function(){
          $('.nav--InsideContainer.blog').on("click", function(){
              $(this).toggleClass('blog-info');
          });
        });
    </script>
    <style type="text/css">
        body.mobile .nav--InsideContainer.blog.blog-info:before {
            opacity: 1;
        }
        body.mobile .nav--InsideContainer.blog.blog-info .subMenu__list--link,
        body.mobile .nav--InsideContainer.blog.blog-info .nav--InsideContainer__img {
            opacity: 0.1;
        }
    </style>
<?php endif; ?>

</nav>