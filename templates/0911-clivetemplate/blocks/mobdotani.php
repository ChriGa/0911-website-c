<?php defined('_JEXEC') or die; ?>
<div class="aniMobDot--container">
    <svg width="300" height="15">
      <rect class="aniMobDot opa02" x="4" y="4" rx="100" ry="100" width="6" height="6" />
      <rect class="aniMobDot opa04" x="12" y="4" rx="100" ry="100" width="6" height="6" />
      <rect class="aniMobDot opa06" x="20" y="4" rx="100" ry="100" width="6" height="6" />
      <rect class="aniMobDot opa01" x="28" y="4" rx="100" ry="100" width="6" height="6" />
      <rect class="aniMobDot opa02" x="36" y="4" rx="100" ry="100" width="6" height="6" />
      <rect class="aniMobDot opa03" x="44" y="4" rx="100" ry="100" width="6" height="6" />
      <rect class="aniMobDot opa01" x="52" y="4" rx="100" ry="100" width="6" height="6" />
      <rect class="aniMobDot opa1" x="60" y="4" rx="100" ry="100" width="6" height="6" />
      <rect class="aniMobDot opa04" x="68" y="4" rx="100" ry="100" width="6" height="6" />
      <rect class="aniMobDot opa01" x="76" y="4" rx="100" ry="100" width="6" height="6" />
      <rect class="aniMobDot opa02" x="84" y="4" rx="100" ry="100" width="6" height="6" />
      <rect class="aniMobDot opa03" x="92" y="4" rx="100" ry="100" width="6" height="6" />
      <rect class="aniMobDot opa06" x="100" y="4" rx="100" ry="100" width="6" height="6" />
      <rect class="aniMobDot opa03" x="108" y="4" rx="100" ry="100" width="6" height="6" />
    </svg>     
  </div>
<style type="text/css">.opa1{opacity:1}.opa05{opacity:.5}.opa02{opacity:.2}.opa07{opacity:.7}.opa01{opacity:.1}.opa04{opacity:.4}.opa08{opacity:.8}.opa06{opacity:.6}.opa03{opacity:.3}</style>