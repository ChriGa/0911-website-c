<?php

/*
* 0911-Webdesign Chris Gabler
*/
defined('_JEXEC') or die;
?>
<div id="indicatorContainer">
	<nav class="nav nav--indicators">
		<button class="nav__item btnStart nav__item--current" ><span class="nav__item-title">Start</span></button>
		<button class="nav__item btnDesign" ><span class="nav__item-title">Design</span></button>
		<button class="nav__item btnPerformance" ><span class="nav__item-title">Performance</span></button>
		<button class="nav__item btnEntwicklung" ><span class="nav__item-title">Entwicklung</span></button>
		<button class="nav__item btnKonzeption" ><span class="nav__item-title">Konzeption</span></button>
	</nav>
</div>