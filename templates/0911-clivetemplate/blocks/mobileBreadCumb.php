<?php 
	defined('_JEXEC') or die;
	$currentUri = $_SERVER['REQUEST_URI'];

	switch($currentUri) : // hier die finalen relativen URL's anpassen!:
		case "/" : $home = true;
			break;
		case "/homepage-design-fuer-ihre-zielgruppe.html" : $design = true;
			break;
		case "/webseite-schneller-machen.html" : $perform = true;
			break;
		case "/homepage-konzept-und-strategie.html" : $konzept = true;
			break;
		case "/webentwicklung-und-programmierung.html" : $entwicklung = true;
			break;			
	endswitch;	
?>
<div class="mobileBreadCrumb--container">
	<nav id="mobileBreadcrumb">
		<a onclick="menueStartReverse()" class="mobHome " href="/">home</a><span>&rarr;</span>
		<a onclick="designMenuStart()" class="mobDesign <?php print($design) ? "active" : (($design || $perform || $konzept || $entwicklung) ? " mobVisible" : " "); ?>"  href="/homepage-design-fuer-ihre-zielgruppe.html">design</a>
			<?php print($perform || $konzept || $entwicklung) ? "<span>&rarr;</span>" : " " ?>
		<a onclick="performanceMenuStart()" class="mobPerf <?php print($perform) ? "active" : (($perform || $konzept || $entwicklung) ? " mobVisible" : " mobHidden"); ?>" href="/webseite-schneller-machen.html">performance</a>
			<?php print($konzept || $entwicklung) ? "<span>&rarr;</span>" : " " ?>		
		<a onclick="konzeptionMenuStart()" class="mobKonz <?php print($konzept) ? "active" : (($konzept || $entwicklung) ? " mobVisible" : " mobHidden"); ?>" href="/homepage-konzept-und-strategie.html">konzeption</a>
			<?php print($entwicklung) ? "<span>&rarr;</span>" : " " ?>		
		<a onclick="entwicklungMenuStart()" class="mobEnt <?php print($entwicklung) ? "active" : " mobHidden" ?>" href="/webentwicklung-und-programmierung.html">entwicklung</a>		
	</nav>
</div>