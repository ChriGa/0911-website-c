/*0911 JS mobile*/
"use strict"

if(navigator.userAgent.match('CriOS')) {
	$('body').addClass('chromeIos');
}
//if($('body').hasClass('chromeIos'))	{alert('body class set!')};

var initLoad = new TimelineMax();

var currentUrlPath = location.pathname,
isHome = false,
themePages = false; 
(currentUrlPath == "/") ? isHome = true : isHome = false;

var allSvgElements = [$('svg circle'), $('svg path:not(#exclude)')],
	startCircleLeft = [$('#main-kreis'), $('#inner-kreis')],
	navDotsAll = [$('#nav-dots-inner-kreis'), $('#nav-dots-right-bottom'), $('g[id^=cross-small] path')],
	arrows = [$('#arrow path'), $('#arrow-2 path')],
	dotsRows = [$('#dots-vertical-kreis-middle'), $('#dots-horizontal-header-bottom'), $('#dots-vertical-kreis-right-bottom'), $('#dots-horizontal-kreis-middle')],
	//dotsVert = $('[id^=dots-vertical] path[id^=dot]'),
	//dotsHoriz = $('[id^=dots-horizontal] path[id^=dot]'),
	//dotsAllDirect = [dotsHoriz],
	rectElements = [$('#vertical-rect-1 path'), $('#header-rect'), $('#header-rect-glitch-bckgr'), $('#rect-corner-element-1'), $('#rect-corner-element-2')],
	rectElementsHoriz = [$('#horizontal-rect path'), $('#horizontal-rect-2 path'), $('#horizontal-rect-header path'), $('#horizontal-rect-right path')],
	rectAll = [rectElements, rectElementsHoriz],
	verticalStrokesLights = [$('stroke-light-vertical-right-1'), $('#stroke-light-vertical-right-2'), $('#stroke-light-vertical-1'), $('#stroke-light-vertical-2')],
	horizontalStrokesLights = $('path[id^=stroke-light-horizontal]'),
	verticalLineHeaderStrike = $('path[id^=vertical-line-header-strike]'),
	centerStroke = $('path[id^=center-stroke]'),
	strokeCrossCenter = [verticalLineHeaderStrike, centerStroke],
	winkelAngelRects = [$('#winkel-angel-rect-1'), $('#winkel-angel-rect-2')],
	lineArrayCircleHeader = [ $('#vertical-line-out-header'), $('#vertical-line-out-kreis-1'), $('#vertical-line-out-kreis-2')],
	lineAngleRight = [ $('#vertical-line-out-header-2'), $('#vertical-line-out-kreis-3'), $('#vertical-line-out-kreis-4')],
	mainKreisStrokes = [$('#main-kreis-middle-stroke'), $('#main-kreis-offset-stroke')],
	startHeaderH1 = $('#startHeader h1'),
	startHeaderH2 = $('#startHeader h2'),
	headerBorderBottom = $('#header-border-bottom path'),
	wrapperRect = $('#wrapper-rect'),
	startBtnWrapper = $('p.startBtn--wrapper'),
	headerSplitText = "", // basic initial - auf Unterseiten sonst error weil nur auf startsseite benötigt
	headerArray = "", // basic initial - auf Unterseiten sonst error weil nur auf startsseite benötigt
	themeHeaderArray = [], // basic initial
	headerProgress = "", // basic initial
	topLine = $('.top'),
    bottomLine = $('.bottom'),
    middleLine = $('.middle'),
    lineText = $('.text'),
	hamLines = [topLine, middleLine, bottomLine],
	downRightVerticalLines = [$('#vertical-line-out-kreis-1_1_'), $('#vertical-line-out-kreis-2_1_')],
	kontaktString = $('#kontaktString'),
    telefonIcon = $('#telefonIcon'),
    umschlagIcon = $('#umschlagIcon'),
    kontaktIcons = [kontaktString, telefonIcon, umschlagIcon],
    themePagesTlCheck = false,
    bodyBefore = CSSRulePlugin.getRule("body.phone.ready:before");

/*unterseiten var's*/

	var innerKreis = $('#inner-kreis'),
		mainKreis = $('#main-kreis'),
		headerRect = $('#header-rect'),
		headerProgressBefore = CSSRulePlugin.getRule(".specialChar h1.headerProgress:before"),
		headerProgressAfter = CSSRulePlugin.getRule(".specialChar h1.headerProgress:after"),
		contentElements = [],
		rectCornerEl = [ 
						$('#rect-corner-element-1'), $('#rect-corner-element-2')
					],
		afterStartElements = [
						$('#wrapper-rect'), $('#vertical-line-out-kreis-1'),
						$('#vertical-line-out-kreis-2'), $('#vertical-line-out-kreis-3'),
						$('#vertical-line-out-kreis-4'), $('#vertical-line-out-header-2'),
						downRightVerticalLines
					];

	// setter für zusätzliche Elemente die nicht für Startseite gebraucht werden
		//TweenMax.set(pfeil, { opacity: 0, stroke: "#484848"});

/*<--- END unterseiten var's*/

	/*Morph var's */
		//MorphSVGPlugin.convertToPath($('#main-kreis'), $('#wrapper-rect'));
		MorphSVGPlugin.convertToPath($('#gear-main-kreis'));
		MorphSVGPlugin.convertToPath($('#gear-inner-kreis'));
		var morphMainKreis = $('#gear-main-kreis'),
			morphInnerKreis = $('#gear-inner-kreis'),
			morphKreises = [$('#gear-main-kreis'), $('#gear-inner-kreis')],
			morphMKrBckp = $('#gear-main-kreis'), //CG: brauchen wir als Backup um morph wieder rückgängig machen zu können
			morphIKrBckp = $('#gear-inner-kreis'),
			zahnradGross = $('#zahnrad-gross'),
			zahnradKlein = $('#zahnrad-klein'),
			lightBulb = $('#lightBulb'),
			polySphere = $('#polyFront-nodes');
		
	/*<--END Morph vars */

	/*splitText vars*/
	if(isHome) {
			headerSplitText = new SplitText(".headerStart", {type:"chars"});
			headerArray = shuffleArray(headerSplitText.chars);
		}
	function themePagesCheck(bool) {
		if($('.item-page').hasClass('specialChar')) {
				//if ( themeHeaderArray != 'undefined') { themeHeaderArray.length = 0; }
				//if (headerProgress != 'undefined') {headerProgress.length = 0;} //CG toDo: array ist im Fortlauf nie wirklich leer- deswegen sind manche header Animationen langsamer
				headerProgress = new SplitText(".headerProgress", {type:"chars"});
				themeHeaderArray = shuffleArray(headerProgress.chars);

				contentElements = [ $('.contentWrapper h2'), $('.contentWrapper p')];
				var duration = 3; 
				(bool) ? duration = 1 : duration = 2; //CG toDo: duration= 1 ist zu langsam im moment bei direkten Aufruf URL performance und konzept 
					TweenMax.delayedCall( duration, singleLetterAni, [themeHeaderArray, headerProgress]);
					TweenMax.staggerFromTo(contentElements, duration + 1.5, { y: 200, alpha: 0 }, { y: 0, alpha: 1, ease: Power4.easeOut, delay: 3}, 0.3 ); //CG: delay und duration wert kürzer bei mobile

						//console.log(duration + 1);

			return themePages = true;
					//alert('yup');
		} else {
			return themePages = false;
				//console.log(themePages);
		 		//alert('nope');
		 }
	}

	/*<-- END splitText vars*/

	initLoad.set(allSvgElements, {scale: 0, transformOrigin: "50% 50%" });

// run preloader code here
	//siehe hier zB https://codepen.io/rfenik/pen/NjPqxW

function loadInitFn() {
		var preLoaderChars = [$('.preLoader--char')],
			d1=[5,0,9,8],d2=[0,9,1,1];
	    	d2.roundProps="0,1,2,3";
	    	d2.onUpdate=update;
	    var preLoadTween = TweenLite.to(d1,1,d2);
	    	preLoadTween.eventCallback("onComplete", init);	

		function init() {
				setTimeout(function(){
					$('body').addClass('ready'); //set Body ready
					//document.getElementById("preLoader").className = 'preLoadDone';
		    		initLoad.play().timeScale(1.2); // debug 9 -> css styles auch wieder umstellen! hamburger .line animation Zeile No. 1430 ca
					//document.getElementById("preLoader").style.display = "none";
		    	}, 800);
	  		}
	    function update(){
	      function R(x){
	        var X = d1[x]<100 ? d1[x]<10 ? ' ':'0':' ';
	        return X+d1[x];
	      };
	        $(preLoaderChars).each(function(i){
	          if( i == 0) {
	            $(this[i]).html(d1[0]).attr("title", d1[0]);
	              i++;
	          }
	          while(i < $(this).length) {
	            $(this[i]).html(R(i)).attr("title", R(i));  
	            i++; 
	          }
	        });
	    };    
    }

// warten bis DOM ready
document.addEventListener("DOMContentLoaded", function(event) {

	window.addEventListener("load", function(e) {

		if(isHome) {
			initLoad.pause();
			loadInitFn();
			setTimeout(function(){ // hamburger lines class remove wg init load animation - nicht mit gsap wg anderer abhängigkeiten 
				$('.line').removeClass('loadAni');
			}, 8500);
		} else {
			$('body').addClass('ready'); //set Body ready
			document.getElementById("preLoader").style.display = "none";
			initLoad.play();
			setTimeout(function(){ // hamburger lines class remove wg init load animation - nicht mit gsap wg anderer abhängigkeiten 
				$('.line').removeClass('loadAni');
			}, 5500);		
		}

	//TweenLite.set(headerRect, { rotationY: 0, transformPerspective: 500, transformOrigin: "50% 50%" });

		initLoad.add('start', 0) //muss das hier definiert sein innerhalb window.addEventlistener?? 
				.set(navDotsAll, {scale: 1}, "start+=0")
					.call(flickerFX, [navDotsAll, 3], this, "start+=0.1")
				.set(dotsRows, {scale: 1}, "start+=1")
				.staggerTo(startCircleLeft, 3, { scale: 1, ease: Expo.easeOut }, 0.5, "start+=1")
				.set(lineAngleRight, { scale:1}, "start+=1.4")
					.fromTo(lineAngleRight, 2, {drawSVG: "0%" }, { drawSVG:"100%",  ease: Power3.easeOut }, "start+=1.4")
				//.fromTo(dotsAllDirect, 1, {scale: 0}, {scale:1,ease: Elastic.easeOut.config(2, 1)}, "start+=1.5")
				.to(rectElements, 1, { scale:1}, "start+=1.5")
				.set(verticalLineHeaderStrike, { scale:1}, "start+=1.8")
					.fromTo(verticalLineHeaderStrike, 2.5, {drawSVG: "0%" }, { drawSVG:"100%",  ease: Power3.easeOut }, "start+=1.8")
				.call(steadyFlickerFX, [$('#header-rect-glitch-bckgr'), 3, 0.2], this, "start+=2")
				//.call(moveIt, [dotsVert, 4, "y"], this, "start+=2")
				//.call(moveIt, [dotsHoriz, 3, "x"], this, "start+=2")
					.fromTo(startHeaderH1, 1.5, { opacity: 0, rotation: "-1", textShadow: "-10px 10px 4px rgba(255,255,255,0.4)" },
							 { opacity: 1, rotation: "0", textShadow: "-20px 0px 2px rgba(255,255,255,0.2)", repeat: 1, ease: RoughEase.ease.config({points:100, taper:"none", strength:2}) }, "start+=2.2" )
					.staggerFromTo( headerArray, 0.5, { opacity: 0, color: "#2962ff" }, { opacity: 1, color: "#fff", repeat: 2 }, 0.1, "start+=2", allDone, [headerSplitText])
				.set(rectElementsHoriz, {scale: 1,scaleX: 0}, "start+=2").to(rectElementsHoriz, 1, { scaleX:1, transformOrigin: "50% 50%" }, "start+=2")
				.fromTo(winkelAngelRects, 1, { scale: 0 }, { scale: 1, stroke: "#fff", rotation: 360, ease: Back.easeOut.config(4) }, "start+=2.2")
				.set(arrows, {scale: 1}, "start+=2.4")
					.call(flickerFX, [arrows, 2], this, "start+=2.5")
					.staggerFromTo(arrows, 2, { y: -10 }, { y: 0, ease:RoughEase.ease.config({points:100, taper:"none", strength:2})}, 0.2, "start+=2.5")		
				.set(lineArrayCircleHeader, { scale:1}, "start+=3")
					.fromTo(lineArrayCircleHeader, 3, {drawSVG: "0%" }, { drawSVG:"100%",  ease: Expo.easeOut }, "start+=3")
				.set(mainKreisStrokes, { scale:1}, "start+=3")
					.fromTo(mainKreisStrokes, 1.5, {drawSVG: "0%" }, { drawSVG:"100%",  ease: Expo.easeIn }, "start+=3")
				.set(centerStroke, { scale:1}, "start+=4")
					.fromTo(centerStroke, 1, {drawSVG: "0%" }, { drawSVG:"100%",  ease: Power3.easeIn }, "start+=4")
				.set(horizontalStrokesLights, { scale: 1}, "start+=4.5")
					.staggerFromTo(horizontalStrokesLights, 6, { x: 0 }, { x: 50, repeat: -1, yoyo:true, ease: Power1.easeInOut }, 1, "start+=4.5")
				.set(verticalStrokesLights, { scale: 1}, "start+=4.5")
					.call(flickerFX, [verticalStrokesLights, 1], this, "start+=4.5")
					.staggerFromTo(verticalStrokesLights, 5, { y: 0 }, { y: 150, repeat: -1, yoyo:true, ease: Power2.easeInOut }, 1, "start+=4.5" )
				.set(wrapperRect, { scale: 1 }, "start+=4.4")
					.fromTo(wrapperRect, 0.5, { drawSVG: "0" }, { drawSVG: "100%", transformOrigin: "50% 50%" }, "start+=4.5")
				.fromTo(startHeaderH2, 1, { opacity: 0}, { opacity: 1, ease:Power3.easeInOut }, "start+=4.5")
				.to(startBtnWrapper, 0.8, {opacity: 1}, "start+=5.5")
				.to(downRightVerticalLines, 1, { scale: 1 }, "start+=4.5")
				.to(kontaktIcons, 0.5, {  scale: 1 }, "start+=4.9");
				//.staggerFromTo(hamLines, 3, { y: 800}, { y: 0, ease: Elastic.easeInOut.config(1, 0.2), repeat: 1 }, 0.3, "start+=10");

			// dev und debugg: 
				//initLoad.play();
				//GSDevTools.create();

    }, false);			
});	
	function menueStartReverse() { //siehe nav-dot-menu.php onclick-event a-tag
		revertAll();
		if(!$('body').hasClass('back')) {$('body').addClass('back'); $('body').removeClass('btnStart');}
		if($('#wrapper').hasClass('overflowHidden')) {$('#wrapper').removeClass('overflowHidden');}
	}

/*start*/
var svgShapeMorphValue = "M -30.45,-57.86 -30.45,442.6 53.8,443.8 53.8,396.3 179.5,396.3 179.5,654.7 193.3,654.7 193.3,589.1 253.1,589.1 253.1,561.6 276.1,561.6 276.1,531.2 320.6,531.2 320.6,238.6 406.5,238.6 406.5,213.9 435.6,213.9 435.6,246.2 477,246.2 477,289.9 527.6,289.9 527.6,263.3 553.7,263.3 553.7,280.4 592,280.4 592,189.2 742.3,189.2 742.3,259.5 762.2,259.5 762.2,103.7 776,103.7 776,77.11 791.3,77.11 791.3,18.21 852.7,18.21 852.7,86.61 871.1,86.61 871.1,231 878.7,240.5 878.7,320.3 891,320.3 891,434.3 923.2,434.3 923.2,145.5 940.1,145.5 940.1,117 976.9,117 976.9,139.8 1031,139.8 1031,284.2 1041,284.2 1041,242.4 1176,242.4 1176,282.3 1192,282.3 1192,641.4 1210,641.4 1210,692.7 1225,692.7 1225,599.6 1236,599.6 1236,527.4 1248,527.4 1248,500.8 1273,500.8 1273,523.6 1291,523.6 1291,652.8 1316,652.8 1316,533.1 1337,533.1 1337,502.7 1356,502.7 1356,523.6 1414,523.6 1414,491.3 1432,491.3 1432,523.6 1486,523.6 1486,-57.86 Z";

var btnStartTransition = new TimelineMax({paused:true});

		btnStartTransition.set('#svgPath', { scale:1}, "startTrans")
			.to($('body'), 1.8, { backgroundPositionY: "-120vh" }, "startTrans")
			.to($('#svgShape'), 2.9, { yPercent: -200 }, "startTrans")
			.to('#svgPath', 1.5, { morphSVG: svgShapeMorphValue, repeat: 0, yoyo:true }, "startTrans");


/*<---END start*/

/*design*/

var designTransition = new TimelineMax({paused: true});

	designTransition.set(contentElements, { x: 200, alpha: 0 }, "absoluteStart")
					.to(headerRect, 0.05, { scale: 0}, "absoluteStart+=0.1")
					.to(rectCornerEl, 0.05, { scale: 0}, "absoluteStart+=0.1")
					//.to(dotsHoriz, 0.1, { x: -150}, "absoluteStart+=0.1")
					.to(afterStartElements, 1.5, {stroke: "#000", ease: Power3.easeIn }, "startTrans+=1")
					.fromTo(headerProgressBefore, 1.5, {cssRule: { left:-100 }}, { cssRule: { left: 0 }, ease: Bounce.easeOut }, "startTrans+=1")
					.fromTo(headerProgressAfter, 1.5, {cssRule: { left: 50, opacity: 0 }}, { cssRule: { left: 0, opacity: 1 }, ease: Bounce.easeOut }, "startTrans+=1")
						//.fromTo(innerKreis, 0.1, { x: 0 }, { x: 230 }, "startTrans+=1.2")						
						//.fromTo(mainKreis, 0.1, { x: 0 }, { x: 420 }, "startTrans+=1.2")
						.to(startCircleLeft, 0.5, { stroke: "#000"}, "startTrans+=1.2")
						.to(innerKreis, 1, { attr:{ r: 190 }}, "startTrans+=1.2")
						.to(mainKreis, 1.6, { attr:{ r: 630 }}, "startTrans+=1.2")					
					.to(winkelAngelRects, 2, { y: -15, stroke: "#484848", scale: 1, rotation: 720, ease: Bounce.easeOut }, "startTrans+=0.5")
					.to(downRightVerticalLines, 1, { stroke: "#000" } );

/*menue link onClick function(geht nur so, siehe nav-dot-menu.php):*/
	function designMenuStart() {
		designTransition.play(2); //CG: oder restart() wenn play probleme macht
		btnStartTransition.progress(1, false);
		//btnStartTransition.restart(); //CG: bei Nav innerhalb unterseiten brauchen wir diese Ani? 
		performanceTransition("back");
		if(!$('body').hasClass('btnStart')) {$('body').addClass('btnStart'); $('body').removeClass('back');}
		if($('body').hasClass('start')) {$('body').removeClass('start');} //CG: wg Hamburger menu color
		if($('#wrapper').hasClass('overflowHidden')) {$('#wrapper').removeClass('overflowHidden');}
	}
/*<--- END menue link onClick function*/

/*<--END design*/

/*performance*/

	function performanceTransition(parameter) { //CG: als Funktion weil weitere Tweens mit main -und innerkreis die Elemente absolut verändert (zB wenn man von performance.html auf home wechselt), die anderen TLs evtl auch als function? 
		var tl = new TimelineMax({paused: true});
			tl.add("absoluteStartPerf", 0)			
				.set(innerKreis, { attr:{ r: 0 }}, "absoluteStartPerf")
				.set(mainKreis,  { attr:{ r: 0 }}, "absoluteStartPerf")				
				.fromTo(morphKreises, 0.1, { visibility: "hidden"}, { visibility: "visible"}, "absoluteStartPerf")
				//.to(winkelAngelRects, 2, {y: -15, stroke: "#484848", scale: 1, rotation: 0, ease: Bounce.easeOut }, "absoluteStartPerf+=0.5")
				.set(morphMainKreis, { attr: {r: 630 }}, "absoluteStartPerf")
				//.to(morphMainKreis, 2, {x: 0,  morphSVG: {shape: zahnradGross, precompile:"log"}}, "absoluteStartPerf+=1.5")
				.to(morphMainKreis, 2, { x: 0, morphSVG: zahnradGross}, "absoluteStartPerf+=1")
				.to(morphInnerKreis, 2, { opacity: 0.3, x: 0, morphSVG: zahnradKlein}, "absoluteStartPerf+=1")
				.fromTo(headerRect, 0.5, { scaleX: 1, rotationY: 0 }, {x: 30, scaleX: 1.5, rotationY: 180}, "absoluteStartPerf+=1")
					.to(morphMainKreis, 180, {rotation: 360, transformOrigin: "51% 50%", ease:Linear.easeNone, repeat:-1}, "absoluteStartPerf+=2")
					.to(morphInnerKreis, 120, {rotation: -360, transformOrigin: "51% 50%", ease:Linear.easeNone, repeat:-1}, "absoluteStartPerf+=2")
				.fromTo(headerProgressBefore, 1.5, {cssRule: { left:-100 }}, { cssRule: { left: 0 }, ease: Bounce.easeOut }, "absoluteStartPerf+=2")
				.fromTo(headerProgressAfter, 1.5, {cssRule: { left: 50, opacity: 0 }}, { cssRule: { left: 0, opacity: 1 }, ease: Bounce.easeOut }, "absoluteStartPerf+=2");
			if(parameter == "play") {
				return tl.restart();
			}else if(parameter == "back") {
				TweenMax.set(morphMainKreis, { x: 0, rotation: 0, morphSVG: morphMKrBckp});
				TweenMax.set(morphInnerKreis, {x: 0, rotation: 0, morphSVG: morphIKrBckp});
				return tl.totalTime(0);				
			}
	}

	function performanceMenuStart() {
		designTransition.progress(1, false);
		btnStartTransition.progress(1, false);
		performanceTransition("play");
		//konzeptionTransition("back"); //CG darf nicht sonst gibts kein Morph! 
		if(!$('body').hasClass('btnStart')) {$('body').addClass('btnStart'); $('body').removeClass('back');}
		if($('body').hasClass('start')) {$('body').removeClass('start');} //CG: wg Hamburger menu color
		if($('#wrapper').hasClass('overflowHidden')) {$('#wrapper').removeClass('overflowHidden');}
	}

/*<---END performance*/

/*konzeption*/

	function konzeptionTransition(parameter) {
		var tl = new TimelineMax({paused:true});
			tl.add("absoluteStartKonz", 0)
				.set(innerKreis, { attr:{ r: 0 }}, "absoluteStartKonz")
				.set(mainKreis,  { attr:{ r: 0 }}, "absoluteStartKonz")
					.to(morphMainKreis, 1, {rotation: 0 }, "absoluteStartKonz")
					.to(morphInnerKreis, 1, {rotation: -0, opacity: 0.3 }, "absoluteStartKonz")		
				.fromTo(morphKreises, 0.1, { visibility: "hidden"}, { visibility: "visible"}, "absoluteStartKonz")
				.to(winkelAngelRects, 2, { y: -15, stroke: "#484848", scale: 1, rotation: 0, ease: Bounce.easeOut }, "absoluteStartKonz+=0.5")
				.fromTo(headerRect, 0.5, {x: 0, scaleX: 1, rotationY: 0 }, {x: 30, scaleX: 1.3, rotationY: 180}, "absoluteStartKonz+=0.5")
				.to(morphMainKreis, 1, { x: 0, rotation: 0, morphSVG: morphMainKreis}, "absoluteStartKonz+=1")
				.to(morphInnerKreis, 2, { x: 0, morphSVG: lightBulb }, "absoluteStartKonz+=1")
				.fromTo(headerProgressBefore, 1.5, {cssRule: { left:-100 }}, { cssRule: { left: 0 }, ease: Bounce.easeOut }, "absoluteStartKonz+=2")
				.fromTo(headerProgressAfter, 1.5, {cssRule: { left: 50, opacity: 0 }}, { cssRule: { left: 0, opacity: 1 }, ease: Bounce.easeOut }, "absoluteStartKonz+=2");

			if(parameter == "play") {
				return tl.restart();
			} else if(parameter == "back") {
				TweenMax.to(morphMainKreis, 1, { x: 0, rotation: 0, morphSVG: morphMainKreis});
				TweenMax.set(morphInnerKreis, {x: 0, rotation: 0, morphSVG: innerKreis}); //CG: hier? weil gleiche Zeile wie in 244? 
				//TweenMax.set(morphMainKreis, { x: 420, rotation: 0, morphSVG: morphMKrBckp});
				return tl.totalTime(0);				
			}
	}

	function konzeptionMenuStart() {
		designTransition.progress(1, false);
		btnStartTransition.progress(1, false);		
		//performanceTransition("back"); //CG darf nicht sonst gibts kein Morph! 
		konzeptionTransition("play");
		if(!$('body').hasClass('btnStart')) {$('body').addClass('btnStart'); $('body').removeClass('back');}
		if($('body').hasClass('start')) {$('body').removeClass('start');} //CG: wg Hamburger menu color
		if($('#wrapper').hasClass('overflowHidden')) {$('#wrapper').removeClass('overflowHidden');}
	}

/*<---END konzeption*/

/*entwicklung*/

	function entwicklungTransition(parameter) {
		var tl = new TimelineMax({paused:true});
			tl.add("absoluteStartEnt", 0)
				.set(innerKreis, { attr:{ r: 0 }}, "absoluteStartEnt")
				.set(mainKreis,  { attr:{ r: 0 }}, "absoluteStartEnt")
					.to(morphMainKreis, 1, {rotation: 0 }, "absoluteStartEnt")
					.to(morphInnerKreis, 1, {rotation: -0 }, "absoluteStartEnt")		
				.fromTo(morphKreises, 0.1, { visibility: "hidden"}, { visibility: "visible"}, "absoluteStartEnt")
				.to(winkelAngelRects, 2, { stroke: "#484848", scale: 1, rotation: 0, ease: Bounce.easeOut }, "absoluteStartEnt+=0.5")
				.fromTo(headerRect, 0.5, {x: 0, scaleX: 1, rotationX: 0 }, {x: 30, scaleX: 1.3, rotationY: 180}, "absoluteStartEnt+=0.5")
				.to(morphMainKreis, 1, { x: 0, rotation: 0, morphSVG: morphMainKreis}, "absoluteStartEnt+=1")
				.to(morphInnerKreis, 2, { x: 0, morphSVG: polySphere }, "absoluteStartEnt+=1")
				.fromTo(morphInnerKreis, 0.5, { opacity: 0.3 }, { opacity: 0.1 }, "absoluteStartEnt+=1.1")
				.fromTo(headerProgressBefore, 1.5, {cssRule: { left:-100 }}, { cssRule: { left: 0 }, ease: Bounce.easeOut }, "absoluteStartEnt+=2")
				.fromTo(headerProgressAfter, 1.5, {cssRule: { left: 50, opacity: 0 }}, { cssRule: { left: 0, opacity: 1 }, ease: Bounce.easeOut }, "absoluteStartEnt+=2")
				.staggerFromTo($('#polyDots circle'), 2.5, { scale: 0, opacity: 0 }, { scale: 1, opacity: 0.3, ease: Elastic.easeOut.config(2.5, 1) }, 0.05, "absoluteStartEnt+=2.5")
				.staggerFromTo($('#polyDots circle'), 2.5, { strokeWidth: "0", stroke: "#2962ff"}, { strokeWidth: "100", stroke: "transparent"}, 0.1, "absoluteStartEnt+=2.5" );
				//.staggerFromTo($('#polyDots circle'), 0.8, {scale: 1}, {scale: 0, yoyo:true, repeat: -1, ease: Elastic.easeIn.config(2.5, 1)}, 0.2, "absoluteStartEnt+=4");

			if(parameter == "play") {
				return tl.restart();
			} else if(parameter == "back") {
				TweenMax.to(morphMainKreis, 1, { x: 0, rotation: 0, morphSVG: morphMainKreis});
				TweenMax.set(morphInnerKreis, {x: 0, rotation: 0, morphSVG: innerKreis}); //CG: hier? weil gleiche Zeile wie in 244? 
				//TweenMax.set(morphMainKreis, { x: 420, rotation: 0, morphSVG: morphMKrBckp});
				return tl.totalTime(0);				
			}
	}

	function entwicklungMenuStart() {
		designTransition.progress(1, false);
		btnStartTransition.progress(1, false);
		//performanceTransition("back"); //CG darf nicht sonst gibts kein Morph! 
		entwicklungTransition("play");
		if(!$('body').hasClass('btnStart')) {$('body').addClass('btnStart'); $('body').removeClass('back');}
		if($('body').hasClass('start')) {$('body').removeClass('start');} //CG: wg Hamburger menu color
		if($('#wrapper').hasClass('overflowHidden')) {$('#wrapper').removeClass('overflowHidden');}
	}

/*<---END entwicklung*/

/*
 * URL TimeLine handler
 */
//final Alias der Beiträge in joomla checken und hier ggfs ändern!
	//if(isHome) { revertAll(); } else { contentElements = []; }
	if(currentUrlPath == "/homepage-design-fuer-ihre-zielgruppe.html") {
		btnStartTransition.play(); designTransition.play(); initLoad.restart();
		//moveIt(dotsHoriz, 3, "x", true); 
	} else {
		contentElements.length = 0; 
	} 
	if(currentUrlPath == "/webseite-schneller-machen.html") { 
		btnStartTransition.progress(1, false); designTransition.progress(1, false); performanceTransition("play"); initLoad.restart(); 
		//moveIt(dotsHoriz, 3, "x", true);
	} else {
		contentElements.length = 0; 
	} 
	if(currentUrlPath == "/homepage-konzept-und-strategie.html") {
		btnStartTransition.progress(1, false); designTransition.progress(1, false); konzeptionTransition("play"); initLoad.restart();	
		//moveIt(dotsHoriz, 3, "x", true);		
	} else {
		contentElements.length = 0; 
	}
	if(currentUrlPath == "/webentwicklung-und-programmierung.html") { 
		btnStartTransition.progress(1, false); designTransition.progress(1, false); entwicklungTransition("play"); initLoad.restart(); 
	} else { 
		contentElements.length = 0; 
	} 
/*<-- END URL TimeLine handler*/

/*hamburgerMenu && Polygon Background */

/*var menuTl = new TimelineMax({paused: true});*/
var	hamMenTl = new TimelineMax({paused:true});
    
/*        menuTl.fromTo("#turbulence", 3, { attr:{"baseFrequency": 0.1 } }, { attr:{"baseFrequency": 0 }, repeat:-1, ease: Elastic.easeOut.config(1, 0.3) , yoyo:false });
  
  $('#menu-toggle').hover(function(){      
      menuTl.restart();
  }, function() {
      menuTl.pause();
  });*/
 
  hamMenTl.add("X", 0)
          .fromTo(lineText, 0.1, { x: 0, opacity: 1 }, {x: -100, opacity: 0 })      
          .to(topLine, 0.6, { y: 0, rotationZ: 120, ease: Elastic.easeOut.config(2, 0.75)})
            .to(topLine, 0.1,  { x: 52, ease: Power2.easeIn, delay: -0.1})
            .to(topLine, 0.6, { y: -17, rotationZ: 855, delay: 0})      
          .to(bottomLine, 0.9, { y: -22, x: 12, rotationZ: 225, delay: -0.5});

  hamMenTl.reversed(true);
  
	var tmax_opts = {
	  yoyo: false
	};

	var hamburger_tl = new TimelineMax(tmax_opts),
	hmMenuGridTL = new TimelineMax({paused: true});

	hamburger_tl.to($('#subMenu'), 0.1, { y: 0, opacity: 1, delay: 2.4 })
				.staggerFromTo($('.nav--InsideContainer'), 0.8, { alpha: 0 }, { alpha: 1, ease: Circ.easeOut}, 0.1, "stagger");
	hamburger_tl.reversed(true);

	hmMenuGridTL.set($('#svgMenuBkgr > rect'), { scale: 0})
				.staggerFromTo($('#svgMenuBkgr > rect'), 1, {fill: "rgba(255,255,255, 0)", scale: 0, x: 1200, y: -300, skewY: 270, rotation: 100 },{fill: "rgba(29,29,29, 1)", scale: 1, x: 0, y: 0, skewY: 0, rotation: 0, ease: Power1.easeInOut, delay: 0.2}, 0.005)
				.fromTo($('#svgGrid--container svg'), 1, {x: 400, scale: 2, rotationY: 45 }, { x: 0, scale: 1, rotationY: 0, delay: -0.9}, "backTag")
				.staggerFromTo($('#svgMenuBkgr > rect'), 0.1, { fill: "rgba(255,255,255, 0.6)" }, { fill: "rgba(29,29,29, 1)", repeat: 1, delay: -0.3 }, 0.005)
				.staggerFromTo($('#svgMenuBkgr > rect'), 0.3, {y: -10, stroke: "#fff" }, { y: 0, stroke: "#1d1d1d" }, 0.005);
	hmMenuGridTL.reversed(true);


//CG nich cool, zwei mal click-function für button#menu-toggle, ändern demnächst: 
	$('button#menu-toggle, .subBreadLink').click(function(){
	     hmMenuGridTL.reversed() ?  hmMenuGridTL.restart() : hmMenuGridTL.reverse().tweenTo("backTag+=0.1").eventCallback("onComplete", tlReverse);
	     $('#subMenu').scrollTop(0);
	     if(!hmMenuGridTL.reversed()) {
		     	$('#svgGrid--container').css("z-index", "3");
		     	//$('body').css('overflow-y', 'hidden');
		     	$('#svgGrid--container').addClass('ifPortraitMobile');
	 	}/*else {
	 		setTimeout(function(){
		     			
		     }, 1200);
	 	}*/
	});

function tlReverse() {
	$('#svgGrid--container').css("z-index", "-1");
	//$('body').css('overflow-y', 'scroll');
	$('#svgGrid--container').removeClass('ifPortraitMobile');
	hmMenuGridTL.resume(0).pause();
}

	$('button#menu-toggle').click(function(event) {
		event.preventDefault();
		    toggleDirection();  
				$(this).toggleClass('openAnimation');
				//$('#wrapper').toggleClass('blur');
				$('#subMenu').toggleClass('hamOpen');
				$('#wrapper').toggleClass('overflowHidden');
				//$('#polygonBckgr').addClass('startAni');
					if (hamburger_tl.reversed()) { 					
					    	hamburger_tl.play();
					} else {
					    hamburger_tl.reverse(2);						   
					}
	});

	$('.subBreadLink').click(function(){ //CG: alles rückgängig wenn click auf subMenu-hamburger-breadcrumb-link 
		$('#subMenu').removeClass('hamOpen');
		$('button#menu-toggle').removeClass('openAnimation');
		hamburger_tl.reverse(2);
		toggleDirection();
	});	
//<------ END zwei mal click-function für button#menu-toggle


	function toggleDirection() {
	      hamMenTl.reversed() ? hamMenTl.play().timeScale(0.8) : hamMenTl.reverse("X"); TweenLite.set(bottomLine, { y: 0 });
	  }  
  
/*<--- END hamburgerMenu && Polygon Background */

/*Kontakt Icons ani / Load*/
      var telPlusUmIconArray = [telefonIcon, umschlagIcon],
      kontaktMorphTl = new TimelineMax({paused: true}); 
      
        TweenMax.set(telPlusUmIconArray, { drawSVG: 0});
  
        kontaktMorphTl.fromTo(kontaktString, 1, { drawSVG: "100%" }, { drawSVG: "0%",ease: Power4.easeOut })
                      .fromTo(telefonIcon, 0.8, { drawSVG: "0%" }, { drawSVG: "100%",ease: Power4.easeIn }, "kontaktString-=0.3")
                      .fromTo(umschlagIcon, 0.8, { drawSVG: "0%" }, { drawSVG: "100%",ease: Power4.easeIn }, "kontaktString-=0.3");
    
        kontaktMorphTl.reversed(true);
    // css ani und tween kombiniert, aufwendig, aber siehe hierzu style.css anhand der Klassen:
        $('.close-kontaktIcons').click(function(){
            toggleKontaktDraw();
            kontaktMorphTl.reversed() ? $(this).css('opacity', '0') : $(this).css({'opacity':'1', 'width':'50px'});
            $('#kontaktWrapper').toggleClass('kontaktSelect');
        });
        $('.close-kontaktIcons--link').click(function(){
        	kontaktMorphTl.reverse();
        	$('#kontaktWrapper').removeClass('kontaktSelect');
        	$('.close-kontaktIcons').css('opacity', '0');
        });
    
      function toggleKontaktDraw() {
          kontaktMorphTl.reversed() ? kontaktMorphTl.play() : kontaktMorphTl.reverse();
      }      
/*<--- END Kontakt Icons ani / Load*/

/*kontakt transition*/

function subMenuPageCheck(currentUrlPath) { // SWITCH Block besser, wenn mehr Bedingungen dazukommen?

	if(currentUrlPath == "/kontakt.html") {
		var kontaktTl = new TimelineMax();
			kontaktTl.staggerFromTo($('.translate'), 2, { y: 100, opacity: 0 }, { y: 0, opacity: 1,ease: Expo.easeOut }, 0.3);
			//$('body').addClass('kontakt');
			$('.kontaktBack').click(function(){ $('body').removeClass('kontakt'); kontaktTl.reverse(); });
			$('.articleBody').scrollTop(0);
	} else if(currentUrlPath != "/webentwicklung-und-programmierung.html") {
			TweenMax.to($('#polyDots circle'), 1, { scale: 0, opacity: 0 });
	}
	var pathPlain = ['/kontakt.html', '/impressum.html', '/datenschutz.html', '/suchen.html']; //CG: hier kommen alle unterseiten URIs rein welche die kontakt-transition verwenden
	var unterseite = false;
	$.each(pathPlain, function(i) {
		if(currentUrlPath == pathPlain[i]) {
			unterseite = true;
		}
	});
	if(!unterseite) {
		if($('body').hasClass('kontakt')) { $('body').removeClass('kontakt');}
		if($('body').hasClass('suche')) { $('body').removeClass('suche');}		
	}	
}

/*<--- END kontakt transition*/	

/*Functions*/

/* 
 * TweenMax.fromTo("#turbulence", 3, { attr:{"baseFrequency":0.01 } }, { attr:{"baseFrequency":0 }, repeat:-1, ease: Elastic.easeOut.config(1, 0.3) , yoyo:true });
 * ACHTUNG: wenn verwenden dann Element mit style Attribute versehen! zB main-kreis: <circle id="main-kreis" class="st1" cx="343.4" cy="480.9" r="478.3" style="filter: url(#displacementFilter)"/>
 */

function singleLetterAni(array, header) {
	TweenMax.staggerFromTo(array, 1, { opacity: 0, color: "#2962ff" }, { opacity: 1, color: "#484848", repeat: 1 }, 0.1, allDone, [header]);
}
function flickerFX(element, time) {
	TweenMax.fromTo(element, time, {opacity: 0}, {opacity:1, ease:RoughEase.ease.config({points:100, taper:"none", strength:2, clamp:true})})
}
function steadyFlickerFX(element, time, opacityValue) {
	TweenMax.fromTo(element, time, {opacity: 0}, {opacity: opacityValue, yoyo:true, repeat:-1, ease:RoughEase.ease.config({points:50, taper:"out", strength:1, clamp:true})})
}
function randomShake(element, time) {
	TweenMax.fromTo(element, time, {x: 0, y:0 }, {x: 5, y:-5, repeat: -1, yoyo:true, ease: RoughEase.ease.config({points:20, taper:"in", strength:2}) })
}
function moveIt(element, time, dir, bool) {
	if(dir == "x") {
		TweenMax.staggerFromTo(element, time, { x: (typeof bool == "undefined" || !bool) ? 0 : -150 }, { x: (typeof bool == "undefined" || !bool) ? 50 : -100 , repeat:-1, yoyo:true }, 0.3)
		//if(!bool) { TweenMax.to(element, 0.1, { x: 150 }); } else { TweenMax.to(element, 0.1, { x: -150 }); }
	} else if(dir == "y") {
		TweenMax.staggerFromTo(element, time, { y: 0 }, { y: 80, repeat:-1, yoyo:true }, 0.4)
	}

}
function shuffleArray(array) {
	//console.log(array.length);
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
}
function allDone(element) {
    element.revert();
}
function headerShake(element) {
	TweenMax.fromTo($('#headerStart h1'), 1.5, { opacity: 0, rotation: "-1" },{ opacity: 1, rotation: "0", textShadow: "-20px 0px 2px rgba(255,255,255,0.2)", repeat: 1, ease: RoughEase.ease.config({points:100, taper:"none", strength:2}) });
}
function revertAll() {
	btnStartTransition.resume("startTrans").pause();
	designTransition.resume("absoluteStart").pause();
	//performanceTransition.resume("absoluteStartPerf").pause();
	performanceTransition("back");
	konzeptionTransition("back");
	entwicklungTransition("back");	
	if($('body').hasClass('specialChar')) { $('body').removeClass('specialChar'); }	
}
function urlAjaxCheck() {
	//CG: seitens joomla wird mittels pageClassSuffix eine Klasse gesetzt in Body, aber wg Barba nur bei direkt aufruf: also muss hier mit urlAjaxCheck das als eventlistener gelöst werden (im moment nur wg lightBulb-grösse in phone portrait ansicht 01102018)
	//ACHTUNG sollten sich die URIs ändern: hier manuell nachtragen 
	var currentUrlPath = location.pathname,
	urlPaths = ["design", "performance", "konzeption", "entwicklung"];
		switch(currentUrlPath) {
			case "/homepage-design-fuer-ihre-zielgruppe.html" : 
				$('body').addClass('design');
				for (var i = urlPaths.length - 1; i >= 0; i--) {
					if(urlPaths[i] != "design") { 
						$('body').removeClass(urlPaths[i]);
					}
				}; 
			break;
			case "/webseite-schneller-machen.html" : 
				$('body').addClass('performance');
				for (var i = urlPaths.length - 1; i >= 0; i--) {
					if(urlPaths[i] != "performance") { 
						$('body').removeClass(urlPaths[i]);
					}
				}; 
			break;
			case "/homepage-konzept-und-strategie.html" : 
				$('body').addClass('konzeption');
				for (var i = urlPaths.length - 1; i >= 0; i--) {
					if(urlPaths[i] != "konzeption") { 
						$('body').removeClass(urlPaths[i]);
					}
				}; 
			break;
			case "/webentwicklung-und-programmierung.html" : 
				$('body').addClass('entwicklung');
				for (var i = urlPaths.length - 1; i >= 0; i--) {
					if(urlPaths[i] != "entwicklung") { 
						$('body').removeClass(urlPaths[i]);
					}
				}; 
			break;				
		}
}

/*<--- END Functions*/

/*barba*/

	var Frontpage = Barba.BaseView.extend({
	  	namespace: 'frontpage',
		  onEnter: function() {
		      // The new Container is ready and attached to the DOM.
		      currentUrlPath = location.pathname; 	
		      subMenuPageCheck(currentUrlPath);
		      //console.log(currentUrlPath);			      

		  },
		  onEnterCompleted: function() {
		    // The Transition has just finished.

				//dot ani definition
					var aniMobDotsTl = new TimelineMax();
					aniMobDotsTl.staggerFromTo($('.aniMobDot'), 2, { x: 0 }, { x: 50, repeat: -1, yoyo: true }, 0.3); //dot Ani neu mobile	
				//<--- END dot ani definition
		  },
		  onLeave: function() {
		    // A new Transition toward a new page has just started.   	
		    		TweenLite.fromTo($('div#kontaktWrapper'), 1.5, { opacity: 0, x: -50 }, { opacity: 1, x: 0, delay: 2 });  		
		  },
		  onLeaveCompleted: function() {
		    // The Container has just been removed from the DOM.
		
		  }
	});

	// Don't forget to init the view!
	Frontpage.init();

Barba.Dispatcher.on('newPageReady', function() {
	//your listener
	urlAjaxCheck();
/*
 * seit ios 12 notwendig! 
 * wenn body zB overflow hidden, funktioniert weder scrollTop per default noch per jQuery oder JS nicht mehr 
 * entdeckt 10092019 - Beobachten!
 */ 
	$('body').animate({scrollTop: 0}, 500); 


	(location.pathname == "/homepage-design-fuer-ihre-zielgruppe.html") ? themePagesTlCheck = false : themePagesTlCheck = true;
		
		themePagesCheck(themePagesTlCheck); //check für h2 splitText effekt und array						

      	$('.startBtn').click(function() {		      		
			btnStartTransition.restart();
			designTransition.restart();
		    	//moveIt(dotsHoriz, 3, "x", true); // damit die dots animieren an neuer position
			if(!$('body').hasClass('btnStart')) {$('body').addClass('btnStart'); $('body').removeClass('back');}
			if($('body').hasClass('start')) {$('body').removeClass('start');} //CG: wg Hamburger menu color			
		});
		$('.homeBtn, .mobHome').click(function() {						
			revertAll();
				//moveIt(dotsHoriz, 3, "x", false); // damit die dots wieder animieren an ursprünglicher position
			if(!$('body').hasClass('back')) {$('body').addClass('back start'); $('body').removeClass('btnStart');}									
			//initLoad.play();		
		});
		/*
		 * merke: bei ajax mit gsap muss für die richtige Referenzierung ein "eigener" Button "geladen" werden - Erinnerung an morph-Problem bei design od konzept fresh-reload bis 21072018 
		 */
		$('.mobDesign').click(function(){
			designTransition.restart();
		});
		$('.nextBtn--design, .mobPerf').click(function(){
			performanceTransition("play");
			//performanceMenuStart();
		});
		$('.nextBtn--performance, .mobKonz').click(function(){
			konzeptionTransition("play");			
			//konzeptionMenuStart();
		});
		$('.nextBtn--entwicklung, .mobEnt').click(function(){
			entwicklungMenuStart("play");
			//konzeptionMenuStart();
		});		


	/*barba rdy document & navDot (hover in JQUERY) */
/*		$(function(){ später löschen, wird nicht gebraucht für mobile

			var ellipseLinkParents = [],
				ellipseLinkParents = $('.ellipseLinkParent'),
				ellipseLinkChilds = [],
				ellipseLinkChilds = $('.ellipseLink'),
				ellipseLinkLines = [],
				ellipseLinkLines = [$('#home-line'), $('#design-line'), $('#performance-line'), $('#konzept-line')];			

			$(ellipseLinkParents).each(function (i) {
			    $(this).hover(function (e) {			    	
			    	setTimeout(function(){
			        	$(ellipseLinkChilds[i]).addClass("dotLinkActive");
			    	}, 500);
			        	$(ellipseLinkParents[i]).hover(over, out);
							animationHandler(ellipseLinkLines[i]);

						$(ellipseLinkParents[i]).click(function(e){
								e.preventDefault();
								$(ellipseLinkChilds[i])[0].click();	
						});						
			    }, function () {
			    		setTimeout(function(){
			            	$(ellipseLinkChilds[i]).removeClass("dotLinkActive");
			        	}, 500);
			            outLine(ellipseLinkLines[i]);
			    });
			});				
			
			//nav dot hover timelines
			var ellipseLinkDist = new TimelineMax({paused: true});
				ellipseLinkDist.fromTo("#turbulence", 3, { attr:{"baseFrequency": 0.5 } }, { attr:{"baseFrequency":0 }, repeat:-1, ease: Elastic.easeOut.config(1, 0.3) , yoyo:false });
				TweenLite.set(ellipseLinkLines, { scale: 1 });
				TweenLite.set(ellipseLinkLines, { drawSVG:"0%"});		
			//<----END nav dot hover timelines

				function animationHandler(element) {
					TweenLite.set(ellipseLinkLines, { drawSVG:"0%"}); //CG: wenn hover unterbrochen bleibt letzte line auf ihren letzten Wert stehen
					var tl = new TimelineMax();
						tl.fromTo(element, 0.7, { drawSVG: "100% 100%", visibility: "hidden" }, { drawSVG: "0% 100%", visibility: "visible", ease: Power3.easeOut });
					animation = tl;					
					return tl.restart();
				}
				function outLine(){
					this.animation.reverse().timeScale(2);
				}

			function over(){
			  ellipseLinkDist.play();
			  //ellipseLineTl.play();
			}
			function out(){
			 ellipseLinkDist.reverse();
			  //ellipseLineTl.reverse();

			}			
			
			$('.ellipseLink').hover(over, out);

			
			//requestAnimationFrame ruft vor jedem erneuten Rendern des Browserfensters
			//die Animations-Funktion auf und erzeugt so einen weichen Übergang von einem Frame zum nächsten. 
			 
				requestAnimationFrame(over, out);    

			// rippleBtnFx 
				rippleBtnFx(); später löschen, wird nicht verwendet bei mobile 
				requestAnimationFrame(rippleBtnFx);

		});*/ /*<---END barba rdy document & navDot hover*/	

			/*kontakt / unterseiten transition*/
				let transitionLink = '',
				bodyBeforeTransition = new TimelineMax({paused:true});
						bodyBeforeTransition.fromTo(bodyBefore, 0.1, { cssRule: { scaleX: 0 }}, { cssRule: { scaleX: 1 } } )
											.fromTo(bodyBefore, 1.3, { cssRule: { x: "100%" }}, { cssRule: { x: "0%"}, ease: Expo.easeInOut } );

				let unterseitenTransition = (param) => {
					if(param === "play") {
						bodyBeforeTransition.restart();
					}
					if(param === "reverse") {
						bodyBeforeTransition.progress(0, false);
					}
				}

				$('.subPageTransLink').each(function(i){
					$(this).click('on', function(e){
						transitionLink = $(this).attr('href');
						e.preventDefault();
						unterseitenTransition("play");
						setTimeout(function(){
							window.location = transitionLink;
							unterseitenTransition("reverse");
						}, 1000);			
					});
				});
			//CG eigner Spezialfall wenn die Suche verwendet wird und auf die Suchergebnisseite geleiter wird: 
				$('.search .form-inline').on("keypress", (e) =>{
					let searchWord = $('.search-query').val();
					if (e.which === 13) { 
						e.preventDefault();
						unterseitenTransition("play");
						setTimeout(function(){
							window.location = '/suchen.html?searchword=' + searchWord + '&searchphrase=all';
							unterseitenTransition("reverse");
						}, 1000);						
					}
				});
			/*<--- END kontakt / unterseiten transition*/

			/*set active für SubMenu breadcrumb menu */
				setNavigation();

				function setNavigation() {
				    var path = window.location.pathname;
				    $("#subMenu a").each(function () {
				        var href = $(this).attr('href');
				        if (path === href) {
				            $(this).addClass('active');
				        } else {
				        	$(this).removeClass('active');
				        }
				    });
				}
			/*<--- END set active für SubMenu breadcrumb menu */
		
		/*bei Browser vor / zurück seite neuladen weil Tweens wg Barba nicht getriggert werden: */
			window.onpopstate = function() {
			  this.location.reload();
			}
		/*<--- END Browser Nav force */				

	});	/*<--END barba*/

//https://greensock.com/sequence-video


// TO DO'S:
// elemente in themePagesCheck in timeline einfügen können?? 
// ******** UNBEDINGT DIE AUSKOMMENTIERTEN LINES AM ENDE LÖSCHEN! SIND ALT ODER VON DER DESKTOP VERSION - UM DEN CODE HIER EINFACH AUSZUDÜNNEN! *******
