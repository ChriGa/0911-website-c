<?php

/**
 * Hier werden Hilfsfunktionen und Variablen definiert die Systemweit durch include
 * in der index.php verwendbar und aufrufbar sind
 * Author: CG
 **/

/**
 * Debuggingausgabe von Objekten und Arrays
 **/	
	function preprint($s, $where="") {
		print "<pre>";
			if ($where) print $where."<br/>";
			print_r($s);
		print "</pre>";
	}

// Sind wir auf der Startseit?: http://docs.joomla.org/How_to_determine_if_the_user_is_viewing_the_front_page
$is_home = false;
$menu = JFactory::getApplication()->getMenu(); 
if ($menu->getActive() == $menu->getDefault()) { $is_home = true; }

/**
 * Meta Generator löschen und 089-webdesign eintragen
 **/
	unset($headMetaData["metaTags"]["standard"]["title"]); // macht hier Probleme? bisher lokal, live Webspace Nein
	$this->setGenerator('0911-Webdesign'); 

/*erweiterte Mobile Detection siehe: http://mobiledetect.net/*/

		require_once 'Mobile_Detect.php';
		$detect = new Mobile_Detect;
	//mobile detect Variablen
		$isTablet = false;
		if (!$detect->isMobile()) {	$agent = "desktop "; $isMobile = false; $isDesktop = true; }
		elseif (!$detect->isMobile() || $detect->isTablet()) { $agent = "tablet "; $isMobile = true; $isTablet = true; /*21052019 macht Probleme keine Lösung bisher: $isTablet = true;*/ } 
		else { $agent = "phone "; $isMobile = true; $isPhone = true; }
		
// CG benötigen pageclass als Body-Klasse
	$beitragsID = JRequest::getVar('Itemid');
	$menu4PageClass = JFactory::getApplication()->getMenu();
	$activeBeitrag = $menu4PageClass->getItem($beitragsID);
	$paramsBeitrag = $menu4PageClass->getParams( $activeBeitrag->id );
if (!empty($paramsBeitrag->get( 'pageclass_sfx' ))) {
	$pageclass = $paramsBeitrag->get( 'pageclass_sfx' );
} else {
	$pageclass = " ";
}

//openGraph properties (basic) siehe: http://ogp.me

$doc = JFactory::getDocument();
$siteMetaDesc = $doc->getMetaData('description');
$siteMetaKeywords = $doc->getMetaData('keywords');

$og_image = $this->params->get('og_image', 'images/ogimage.jpg' );

		// Typ dieser Seite - normalerweise "website"
			if (!isset($headMetaData["metaTags"]["property"]["og:type"]) OR empty($headMetaData["metaTags"]["property"]["og:type"])) {
				$headMetaData["metaTags"]["property"]["og:type"] = "website";
			}

		// OG:title ist normalerweise dasselbe wie der Meta-Title
			if (!isset($headMetaData["metaTags"]["property"]["og:title"]) OR empty($headMetaData["metaTags"]["property"]["og:title"])) {
				$headMetaData["metaTags"]["property"]["og:title"] = str_replace(array("'", '"'), "", $this->getTitle());
			}
			
		// URL - URL aktuellen Seite 
			if (!isset($headMetaData["metaTags"]["property"]["og:url"]) OR empty($headMetaData["metaTags"]["property"]["og:url"])) {
				$headMetaData["metaTags"]["property"]["og:url"] = "http://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
			}
			
		// Dasselbe wie Meta-Description
				$headMetaData["metaTags"]["property"]["og:description"] = $siteMetaDesc;
			
		// Vorschaubild für Facebook
			if (!isset($headMetaData["metaTags"]["property"]["og:image"]) || empty($headMetaData["metaTags"]["property"]["og:image"])) {
				if (!empty($og_image)){
					$headMetaData["metaTags"]["property"]["og:image"] = "http://".$_SERVER["HTTP_HOST"]."/".$og_image;
				}
				else{
					$headMetaData["metaTags"]["property"]["og:image"] = "http://".$_SERVER["HTTP_HOST"]."/images/ogimage.jpg";
				}
			}
		// OG:image width und height
			$headMetaData["metaTags"]["property"]["og:image:width"] = "1200";
			$headMetaData["metaTags"]["property"]["og:image:height"] = "630";
			$headMetaData["metaTags"]["name"]["keywords"] = $siteMetaKeywords;
		// die Meta-Daten wieder in das Joomla-Array schreiben.
			JDocumentHTML::setHeadData($headMetaData);


/*
 * error-reporting nur wenn Entwicklung local oder live mit eigener IP
 * Immerwieder erneuern! IP oder HTTP-HOST (joomla Version zB)
 */
		
		$user = 'none';

		if ($_SERVER["REMOTE_ADDR"] == "84.153.219.242") {
				$user = "ftp_dev";
		}

		if ($_SERVER["HTTP_HOST"] == "localhost:8888") {
				$user = "local_dev";
		}

			define('dev_umgebung', $user);

		switch(dev_umgebung) {
			case 'ftp_dev':
				error_reporting(E_ALL ^ E_NOTICE);
				ini_set('display_errors', '1');
				break;
			case 'local_dev':
				error_reporting(E_ERROR);
				ini_set('display_errors', '1');
				break;
			case 'none':
				error_reporting(0);
				break;
			default:
				exit('Error-Reporting User wurde nicht korrekt initialisiert!');
				@mail("cg@089webdesign.de", "Vorsicht, bei ".$_SERVER["HTTP_HOST"], __FILE__."\n\nwurde kein User gesetzt für das Error-Reporting!");
		}

/**
 * Für bestimmte Browser im Body eine Klasse setzen, um CSS-Styles setzen zu können
 **/
	$browser = getBrowser(); // siehe /defines.php

	// Name des Browsers
	$br_name = $browser["name"];
	if ($br_name=="Mozilla Firefox") $br_name = "Firefox";
	$br_platform = $browser["platform"];
	
	$browser = $br_name." ".$br_platform." "; // zB "InternetExplorer InternetExplorer90" oder "Firefox Firefox180"
	// Add the menuitem Pageclass-Suffix to the body-class
	$body_class .= " ".$browser." ";
	if($active_menu_item) {
		$body_class .= $active_menu_item->params->get( 'pageclass_sfx' );
	}

?>