<?php 
/**
 * @author   	info@0911-webdesign.com
 * @copyright  	info@0911-webdesign.com
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
?>
	<meta http-equiv="X-UA-Compatible" content="IE=IE11" />
	<link rel="stylesheet" type="text/css" href="<?php print '/templates/' . $this->template . '/css/normalize.css'; ?>">
	<link rel="stylesheet" type="text/css" href="<?php print '/templates/' . $this->template . '/css/styles.css'; ?>">
	<link rel="stylesheet" type="text/css" href="<?php print '/templates/' . $this->template . '/css/responsive.css';?>">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>